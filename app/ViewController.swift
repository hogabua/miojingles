//
//  ViewController.swift
//  MioJingles
//
//  Created by Hager Florian on 14.01.20.
//  Copyright © 2020 Florian Hager. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var player: AVAudioPlayer?
    var currentJingle: String?

    func playSound(soundname: String) {
        guard let url = Bundle.main.url(forResource: soundname, withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let player = player else { return }
            currentJingle = soundname
            if soundname == "joker_5050" {
                player.volume = 1
                player.play()
            } else {
                player.volume = 0
                player.play()
                player.setVolume(1, fadeDuration: 1)
            }
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func togglePlayPause(withName soundName: String) {
        if let currentJingle = currentJingle, let player = player {
            if currentJingle == soundName && player.isPlaying {
                player.setVolume(0, fadeDuration: 1)
                self.currentJingle = nil
            } else {
                player.setVolume(0, fadeDuration: 0.5)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.playSound(soundname: soundName)
                }
            }
        } else {
            playSound(soundname: soundName)
        }
    }

    @IBAction func button1Clicked(_ sender: Any) {
        togglePlayPause(withName: "Eröffnungsmelodie - Millionenshow Soundeffect")
    }
    
    @IBAction func button2Clicked(_ sender: Any) {
        togglePlayPause(withName: "Kandidat_kommt")
    }
    
    @IBAction func button3Clicked(_ sender: Any) {
        togglePlayPause(withName: "Kandidat_geht")
    }
    
    @IBAction func button4Clicked(_ sender: Any) {
        togglePlayPause(withName: "frage")
    }
    
    @IBAction func button5Clicked(_ sender: Any) {
        togglePlayPause(withName: "antwort")
    }
    
    @IBAction func button6Clicked(_ sender: Any) {
        togglePlayPause(withName: "richtig")
    }
    
    @IBAction func button7Clicked(_ sender: Any) {
        togglePlayPause(withName: "falsch")
    }
    
    @IBAction func button8Clicked(_ sender: Any) {
        togglePlayPause(withName: "Telefonjoker")
    }
    
    @IBAction func button9Clicked(_ sender: Any) {
        togglePlayPause(withName: "joker_5050")
    }
    
    @IBAction func button10Clicked(_ sender: Any) {
        togglePlayPause(withName: "Publikumsjoker")
    }
    
}

