fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios upload_to_appcenter
```
fastlane ios upload_to_appcenter
```

### ios upload_to_crashlytics
```
fastlane ios upload_to_crashlytics
```

### ios upload_to_all
```
fastlane ios upload_to_all
```

### ios upload_to
```
fastlane ios upload_to
```

### ios upload_release
```
fastlane ios upload_release
```

### ios upload_testversion
```
fastlane ios upload_testversion
```

### ios aab_to_apk
```
fastlane ios aab_to_apk
```

### ios before_each_build
```
fastlane ios before_each_build
```

### ios set_build_version
```
fastlane ios set_build_version
```

### ios downloadEnterpriseDevCerts
```
fastlane ios downloadEnterpriseDevCerts
```

### ios install_bluesource_xcode_templates
```
fastlane ios install_bluesource_xcode_templates
```

### ios upload_nightly
```
fastlane ios upload_nightly
```

### ios increment_version
```
fastlane ios increment_version
```

### ios enterprise
```
fastlane ios enterprise
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
